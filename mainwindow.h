#include <QMainWindow>

class QwtPlotZoomer;
class Plot;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = 0);

private Q_SLOTS:

    void addSeries();
    void exportPlot();
    void enableZoomMode(bool);
    void removeAllSeries();
    void setLogarithmAxis();

private:

    Plot *plot;
    QwtPlotZoomer *zoomer;
};
