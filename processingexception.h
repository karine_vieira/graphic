#ifndef PROCESSINGEXCEPTION_H
#define PROCESSINGEXCEPTION_H

#include <QException>

class ProcessingException : public QException
{
public:
    ProcessingException(QString const& message) :
        message(message)
    {}

    virtual ~ProcessingException()
    {

    }

    void raise() const { throw *this; }
    ProcessingException *clone() const { return new ProcessingException(*this); }

    QString getMessage() const
    {
        return message;
    }
private:
    QString message;
};

#endif // PROCESSINGEXCEPTION_H
