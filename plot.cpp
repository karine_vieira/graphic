#include "plot.h"

#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_renderer.h>
#include <qwt_scale_engine.h>

class Curve: public QwtPlotCurve
{
public:
    Curve(int index):
        index(index)
    {
        setRenderHint( QwtPlotItem::RenderAntialiased );
    }

private:

    const int index;
};

Plot::Plot(QWidget *parent):
    QwtPlot(parent),
    isDirty(false)
{
    QwtPlotCanvas *canvas = new QwtPlotCanvas();
    canvas->setFocusIndicator(QwtPlotCanvas::CanvasFocusIndicator);
    canvas->setFocusPolicy(Qt::StrongFocus);
    canvas->setPalette(Qt::white);
    setCanvas(canvas);

    setAutoReplot(false);

    setTitle("Legend Test");
    setFooter("Legend");

    // grid
    QwtPlotGrid *grid = new QwtPlotGrid;
    grid->enableXMin(true);
    grid->setMajorPen(Qt::gray, 0, Qt::DotLine);
    grid->setMinorPen(Qt::darkGray, 0, Qt::DotLine);
    grid->attach(this);

    // axis
    //    setAxisScale(QwtPlot::yLeft, 0.0, 10.0);
    //    setAxisScale(QwtPlot::xBottom, 0.0, 10.0);

    insertLegend(new QwtLegend(),QwtPlot::BottomLegend);

    // NOTE: Add zoom.
}

Plot::~Plot()
{
}

void Plot::setAxisTitles(const QString &xlabel, const QString &ylabel)
{
    setAxisTitle(QwtPlot::Axis::xBottom, xlabel);
    setAxisTitle(QwtPlot::Axis::yLeft, ylabel);
}

void Plot::insertCurve(const QVector<double> &x, const QVector<double> &y, const QString &label)
{
    static int counter = 1;
    long int r = random() % 255;
    long int g = random() % 255;
    long int b = random() % 255;

    QwtPlotCurve *curve = new Curve(counter++);
    curve->setPen(QColor(r, g, b));
    curve->setTitle(label);

    QVector<QPointF> points;
    for (int i = 0; i < x.count(); i++){
        points << QPointF(x[i], y[i]);
    }

    curve->setSamples(points);

    // NOTE: Update axis scales with new curve values
    // (all points should be in the widget area).
    setAxisAutoScale(xBottom, true);
    setAxisAutoScale(yLeft, true);
    curve->attach(this);
    this->replot();
}

void Plot::replot()
{
    if (autoReplot()) {
        isDirty = true;
        return;
    }

    QwtPlot::replot();
}

void Plot::clean(){
    QwtPlotItemList curveList = itemList(QwtPlotItem::Rtti_PlotCurve);
    while(!curveList.isEmpty()){
        QwtPlotItem* curve = curveList.takeFirst();
        delete curve;
    }
}

void Plot::removeCurve(const QString &label){
    QwtPlotItemList curveList = itemList(QwtPlotItem::Rtti_PlotCurve);
    for (int i = 0; i < curveList.count(); i++){
        QwtPlotItem* curve = curveList.takeFirst();
        if (curve->title() == label){
            delete curve;
        }
    }
}

void Plot::exportPlot() {
    QwtPlotRenderer renderer;
    renderer.exportTo(this, "graphic.pdf");
}

void Plot::removeAllSeries() {
    QwtPlotItemList curveList = itemList(QwtPlotItem::Rtti_PlotCurve);

    foreach (QwtPlotItem* curve, curveList) {
        delete curve;
    }
}

void Plot::setLogarithmAxis(bool xlog, bool ylog) {
    if (xlog){
        setAxisScaleEngine(QwtPlot::xBottom, new QwtLogScaleEngine(10));
    }
    else {
        setAxisScaleEngine(QwtPlot::xBottom, new QwtLinearScaleEngine());
    }
    if (ylog){
        setAxisScaleEngine(QwtPlot::yLeft, new QwtLogScaleEngine(10));
    }
    else {
        setAxisScaleEngine(QwtPlot::yLeft, new QwtLinearScaleEngine());
    }

    this->replot();
}
