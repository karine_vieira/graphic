#include <QApplication>
#include <QDebug>
#include <QString>
#include <QDialog>
#include <QStringList>
#include "mainwindow.h"
#include "processingexception.h"
#include <QException>
#include <QtDebug>

#ifndef SRC_DIR
#define SRC_DIR "."
#endif

// NOTE: Let's use the Qt standard for coding.
// There is no space before or after a parenthesis.

int main(int argc, char **argv)
{
    try {
        QApplication a(argc, argv);
        MainWindow w;
        w.resize(700, 500);
        w.show();
        return a.exec();
    } catch (const QException& ex) {
        qDebug() << ex.what();
        return 1;
    }

}
