#ifndef CSVFILE_H
#define CSVFILE_H

#include <QString>
#include <QStringList>
#include <QVector>

class CsvFile
{
public:

    // NOTE: Change constructor to receive the fileName and the token (readFile).
    CsvFile(QString &fileName, char token = ',');
    QStringList getFieldNames();
    QVector<double> getDoubleValues(const QString &rowName);

private:

    QStringList fieldNames;
    QVector<QStringList> rows;
    // NOTE: Move to private (use only on the constructor).
    void readFile(QString &fileName, char token = ',');
};

#endif // CSVFILE_H
