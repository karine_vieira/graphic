QT += core gui widgets

CONFIG += c++11

TARGET = graphic
TEMPLATE = app

HEADERS += \
    csvfile.h \
    mainwindow.h \
    plot.h \
    processingexception.h

SOURCES += \
    csvfile.cpp \
    mainwindow.cpp \
    plot.cpp \
    main.cpp

include(/usr/local/Cellar/qwt/6.1.3_3/features/qwt.prf)


#LIBS += -F$$PWD/qwt/lib/ -framework qwt

#INCLUDEPATH += $$PWD/qwt/include
#DEPENDPATH += $$PWD/qwt/include

DEFINES += SRC_DIR=\\\"$$PWD\\\"
