#ifndef _PLOT_H_
#define _PLOT_H_

#include <qwt_plot.h>

class Plot: public QwtPlot
{
    Q_OBJECT

public:

    Plot( QWidget *parent = NULL );
    virtual ~Plot();

public:

    virtual void replot();
    void insertCurve(const QVector<double> &x, const QVector<double> &y, const QString &label);
    void setAxisTitles(const QString &xlabel, const QString &ylabel);
    // NOTE: Use camelCase (isDirty)
    // NOTE: Only private fields.
    bool isDirty;
    void clean();
    void removeCurve(const QString &label);
    void exportPlot();
    void removeAllSeries();
    // NOTE: Rename to setLogarithmAxis
    void setLogarithmAxis(bool xlog, bool ylog);
};

#endif
