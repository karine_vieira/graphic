#include "csvfile.h"
#include "processingexception.h"
#include <QVector>
#include <QFile>
#include <QTextStream>

CsvFile::CsvFile(QString &fileName, char token)
{
    this->readFile(fileName, token);
}

void CsvFile::readFile(QString &fileName, char token) {
    QFile inputFile(fileName);

    if (inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);
        int count = 0;

        while (!in.atEnd()) {
            QString line = in.readLine();
            QStringList row = line.split(token);
            if (count == 0) {
                fieldNames = row;
            } else {
                rows.append(row);
            }
            count++;
        }
        inputFile.close();
    } else {
        throw ProcessingException("Error opening file.");
    }
}

QStringList CsvFile::getFieldNames() {
    return fieldNames;
}

QVector<double> CsvFile::getDoubleValues(const QString &rowName) {
    // NOTE: Add error handling.
    // If the row name ir wrong?
    int i = fieldNames.indexOf(rowName);
    bool check = true;
    if (i < 0) {
        throw ProcessingException("The name " + rowName + " not found.");
    }
    QVector<double> vector;
    foreach (QStringList row, rows) {
        QString s = row.takeAt(i);
        // If the s string could not be converted to double?
        vector.append(s.toDouble(&check));
        if (!check) {
            throw ProcessingException("The value " + s + " is not convertable to double.");
        }
    }
    return vector;
}
