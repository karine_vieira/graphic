#include <QToolBar>
#include <QToolButton>
#include <QLayout>
#include <QVector>
#include <QMessageBox>
#include <QColor>
#include <QDebug>
#include <random>

#include <qwt_plot_zoomer.h>
#include <qwt_plot_canvas.h>

#include "plot.h"
#include "mainwindow.h"
#include "Csvfile.h"
#include "processingexception.h"


bool set = false;

class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer(int xAxis, int yAxis, QWidget *canvas):QwtPlotZoomer(xAxis, yAxis, canvas)
    {
        setTrackerMode(QwtPicker::AlwaysOff);
        setRubberBand(QwtPicker::NoRubberBand);
        setMousePattern( QwtEventPattern::MouseSelect2, Qt::RightButton, Qt::ControlModifier );
        setMousePattern( QwtEventPattern::MouseSelect3, Qt::RightButton );
    }
};

MainWindow::MainWindow(QWidget *parent):QMainWindow(parent)
{
    try {
        plot = new Plot();
        QWidget *box = new QWidget(this);
        QHBoxLayout *layout = new QHBoxLayout(box);
        layout->addWidget(plot, 10);
        setCentralWidget(box);

        // Creates toolbar.
        QToolBar *toolBar = new QToolBar(this);
        addToolBar(toolBar);

        // Creates button.
        QToolButton *btnAddSeries = new QToolButton(toolBar);
        btnAddSeries->setText("Add Series");
        toolBar->addWidget(btnAddSeries);
        connect(btnAddSeries, SIGNAL(clicked()), SLOT(addSeries()));

        QToolButton *btnExport = new QToolButton(toolBar);
        btnExport->setText("Export");
        toolBar->addWidget(btnExport);
        connect(btnExport, SIGNAL(clicked()), SLOT(exportPlot()));

        QToolButton *btnZoom = new QToolButton(toolBar);
        btnZoom->setText("Zoom");
        btnZoom->setCheckable(true);
        btnZoom->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolBar->addWidget(btnZoom);
        connect(btnZoom, SIGNAL(toggled(bool)), SLOT(enableZoomMode(bool)));

        QToolButton *btnRemoveAllSeries = new QToolButton(toolBar);
        btnRemoveAllSeries->setText("Remove All Series");
        toolBar->addWidget(btnRemoveAllSeries);
        connect(btnRemoveAllSeries, SIGNAL(clicked()), SLOT(removeAllSeries()));

        // NOTE: Create button or action to use this method (activate/deactivate logarithm)
        QToolButton *btnActivateLogarithm = new QToolButton(toolBar);
        btnActivateLogarithm->setText("Activate/Desactivate Logarithm");
        toolBar->addWidget(btnActivateLogarithm);
        connect(btnActivateLogarithm, SIGNAL(clicked()), SLOT(setLogarithmAxis()));

        plot->setTitle("Teste");


    } catch(const ProcessingException& ex) {
        QMessageBox message(this);
        message.setText(ex.getMessage());
        message.exec();
        exit(1);
    }
}

void MainWindow::addSeries() {

    QString sourceDir(SRC_DIR);
    // NOTE: Use teste2.csv as test (plot time, all columns)
    // Insert multiples data series.
    // NOTE: Create button to remove all series.
    QString file = sourceDir + "//teste2.tsv";
    CsvFile x(file, '	');
    QString time = "Time";
    QVector<double> listTime = x.getDoubleValues(time);
    QStringList namesCols = x.getFieldNames();
    foreach (QString col, namesCols) {
        if (col == time) {
            continue;
        }
        QVector<double> list = x.getDoubleValues(col);
        plot->insertCurve(list, listTime, time + " x "+ col);
    }

    QString y = "other columns";
    plot->setAxisTitles(y, time);
}

void MainWindow::exportPlot() {
    plot->exportPlot();
}

void MainWindow::enableZoomMode(bool on) {
    if(on){
        if(zoomer != NULL) {
           delete zoomer;
        }

        zoomer = new Zoomer(QwtPlot::xBottom, QwtPlot::yLeft, plot->canvas());
        zoomer->setRubberBand(QwtPicker::RectRubberBand);
        zoomer->setRubberBandPen(QColor( Qt::green));
        zoomer->setTrackerMode(QwtPicker::ActiveOnly);
        zoomer->setTrackerPen(QColor( Qt::white));
    }
    zoomer->setEnabled(on);
    zoomer->zoom(0);
}

void MainWindow::removeAllSeries() {
    plot->removeAllSeries();
}

void MainWindow::setLogarithmAxis() {
    set = !set;
    plot->setLogarithmAxis(set, set);
}
